#!/usr/bin/env python3
"""interpolate values along trajectories"""
import os, sys
from glob import glob
import numpy as np
import pandas as pd
import xarray as xr
from .tools import bilinear_interp_twice


def values_along_parcel(df, ar, lons, lats, levs, times):                 

    times = [np.datetime64(x, 's') for x in times]
    times = np.array(times).astype('f4')

    _lons = df['lon'].values.astype('f4')
    _lats = df['lat'].values.astype('f4')
    _levs = df['lev'].values.astype('f4')
    _times = [np.datetime64(x, 's') for x in df['time'].values]
    _times = np.array(_times).astype('f4')

    vals = []

    for i in df.index:

        _lon = _lons[i]
        _lat = _lats[i]
        _lev = _levs[i]
        _time = _times[i]
        _val = bilinear_interp_twice(ar, lons, lats, levs, times, _lon, _lat, _lev, _time)
        vals.append(_val)

    return vals


if __name__ == '__main__':

    da = xr.open_dataarray('path/to/files')
    lons = da.longitude.values
    lats = da.latitude.values
    levs = da.level.values
    times = da.time.values
    name = 'variable_name'
    ar = da.values

    odir = 'puth/to/output/directory'

    input_csvs = glob('path/to/input/*.csv')

    for f in input_csvs:

        b = os.path.basename(f)
        of = os.path.join(odir, b)
        print(f, '->', of)

        df = pd.read_csv(f)

        df[name] = values_along(df, ar, lons, lats, levs, times)

        df.to_csv(output_csv, index=False, float_format='%g')

