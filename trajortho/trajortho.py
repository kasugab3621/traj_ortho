"""
Atmospheric parcel trajectory using orthodrome v1.1
Copyright (c) 2023 Satoru Kasuga
"""

import os, re
import numpy as np
import pandas as pd
import xarray as xr
from .tools import search_nearest2, bilinear_interp_twice, bilinear_interp, invert_gcd, check_lon_0_360, est_loc_independent, est_loc_orthodrome


class windbox:

    def __init__(self, u, v, w, z,
                 nlon='longitude', nlat='latitude', nlev='level', ntime='time',
                 lons=None, lats=None, levs=None, times=None, xrinterp=False,
                 undef=np.nan, ttimes=None, odir='.'):
        """ 3D gridded boundary box of 3D winds and geopotential height for trajectory calculation

        Positional arguments
        --------------------

        - u, v, w, z:
        Zonal and meridional and vertical components of the vector wind (m/s, m/s, Pa/s)
        and geopotential height (m).
        All components must have the same dimension coordinates.

        If xarray.DataArray, using options of nlon, nlat, nlev, ntime.
        These default sets are for ERA5 from CDS.

        If numpy.ndarray,
        lons (longitude; degree; np.ndarray), lats (latitude; degree; np.ndarray),
        levs (level; hPa; np.ndarray), and times (times; np.datetime64 or pd.data_range()) must be set.


        Optional argument
        --------------

        - undef:
        Undefined values.
        Calculation will be stopped when a parcel reach undef grid.
        Or you can use topography data, see below.

        - xrinterp:
        Flag to choose xarray.DataArray.interp as interpolation method, experimental.
        This may slow down the process, not recomended (if use, xrinterp=True).


        Methods
        -------

        - calc_traj(df, ttimes, (method), (ortho), (odir)):
        Start trajectory calculation and save csvs with automatically allocated number
        for each parcel.

            --- arguments

            - df:
            pandas.DataFrame of Initial parcels with formatted header columns ['lon', 'lat', 'lev']
            for longitue (degree), 'latitude' (degree), 'level' (hPa), respectively.

            - ttimes:
            Timesteps to calculate trajectory.
            This component must be pandas.date_range (or pandas.DatetimeIndex).
            If 'freq' is positive (incremental time steps), foward trajectory analysis will be start.
            If 'freq' is negative (decremental time steps), backward trajectory analysis will be start.
            Available freq unit is limited in hour('h'), minute('min'), and second('S') (see _get_interval_sec).

            --- options

            - odir:
            Output directory path. Current directory is used in default.

            - method:
            Time integral methods to solve ordinal differential equations (ODEs) of motion..
            rk4: 4th order Runge-Kutta method (default)
            rk2: 2nd order Runge-Kutta method
            e: Euler method

            - ortho:
            The way how to locate parcels.
            If True, new locations of longitude and latitude are located along orthodrome by solving ODE in polar coordinate.
            If False, they are located independently by solving ODEs in Cartesian coordinate.
            Values are interpolated by Taylor's first-order approximation.


        - set_topo:
        Set topograpy (m), which is used to check whether parcels go into mountains.
        This component must be numpy.ndarray or xarray.DataArray instances with any shape.
        The coordinates shold be given in the save way as with windbox()
        """

        xrda = xr.DataArray
        nnda = np.ndarray
        pddr = pd.DatetimeIndex

        if isinstance(u, xrda) and isinstance(v, xrda) and isinstance(w, xrda) and isinstance(z, xrda):
            
            self.mode = xrda  # DataArray mode

            # keep meta
            self.ushape = u.shape  

            if u.dims != v.dims or u.dims != w.dims or u.dims != z.dims: 
                msg = 'u, v, w, and z must have the same dimension coordinates'
                raise ValueError(msg)

            names = [nlon, nlat, nlev, ntime]
            unames = [name for name in u.dims]
            vnames = [name for name in v.dims]
            wnames = [name for name in w.dims]
            znames = [name for name in z.dims]

            if set(names) != set(unames) or set(names) != set(vnames) or set(names) != set(wnames) or set(names) != set(znames):
                raise ValueError('wrong vaiable names')

            ucoords = [u.coords[name].values for name in u.dims]
            vcoords = [v.coords[name].values for name in v.dims]
            wcoords = [w.coords[name].values for name in w.dims]
            zcoords = [z.coords[name].values for name in z.dims]

            if not all([(uc == vc).all() for uc, vc in zip(ucoords, vcoords)]):
                raise ValueError('u, v, w, and zmust have the same dimension coordinate values')
            if not all([(uc == wc).all() for uc, wc in zip(ucoords, wcoords)]):
                raise ValueError('u, v, w, and zmust have the same dimension coordinate values')
            if not all([(uc == zc).all() for uc, zc in zip(ucoords, zcoords)]):
                raise ValueError('u, v, w, and zmust have the same dimension coordinate values')

            lons, lats, levs, times = [u[n].values for n in names]

            # convert time to numpy.datetime64
            self.times = u[ntime].values

            if not xrinterp:  # use numpy.ndarray with float32
                print('loading data... u')
                u = u.astype('f4').values
                print('loading data... v')
                v = v.astype('f4').values
                print('loading data... w')
                w = w.astype('f4').values
                print('loading data... z')
                z = z.astype('f4').values

        elif isinstance(u, nnda) and isinstance(v, nnda) and isinstance(w, nnda):

            self.mode = nnda  # ndarray mode
            self.undef = undef

            if xrinterp:
                raise ValueError('xrinterp requires xarray.DataArray inputs')

            if u.shape != v.shape or u.shape != w.shape:
                msg = 'u, v, and w must have the same dimension coordinates'
                raise ValueError(msg)            
            
            #if lons == None or lats == None or levs == None or times == None:
            #    msg = 'coordinates must be set with lons, lats, levs, times options'
            #    raise ValueError(mgs)

            # use numpy.ndarray with float32
            u = u.astype('f4')
            v = v.astype('f4')
            w = w.astype('f4')
            z = z.astype('f4')

            # undef to np.nan
            if undef != None:
                u = np.where(u==undef, np.nan, u)
                v = np.where(v==undef, np.nan, v)
                w = np.where(w==undef, np.nan, w)
                z = np.where(z==undef, np.nan, z)

            # convert time to numpy.datetime64
            if isinstance(times, pd.DatetimeIndex):
                self.times = times.to_numpy()
            else:
                self.times = times
        else:
            raise TypeError('u, v, w, and z must have the same class (xarray.DataArray or numpy.ndarray)')

        # convert coors to numpy.float32
        lons = lons.astype('f4')
        lats = lats.astype('f4')
        levs = levs.astype('f4')

        # init
        self.u = u
        self.v = v
        self.w = w
        self.z = z
        self.lons = lons
        self.lats = lats
        self.levs = levs
        self.topo = 0
        self.xrinterp = xrinterp
        self.odir = odir
        self.ttimes = ttimes

    def set_odir(self, o):
        self.odir = o

    def set_ttimes(self, tt):
        if not isinstance(tt, pd.DatetimeIndex):
            raise ValueError('set_ttimes must be made from pandas.date_range (pandas.DatetimeIndex)')
        self.ttimes = tt

    def set_topo(self, topo, nlon='longitude', nlat='latitude', tlons=None, tlats=None):
        """
        Geopotential height or altitude of the topography.
        This component must be xarray.DataArray or numpy.ndarray instances
        If xarray.DataArray, no need to set coordinates (tlons, tlats), 
          but their shortnames must be set as (nlon, nlat).          
        If numpy.ndarray, set coordinates (tlons, tlats) with numpy.ndarray.
          The shortnames (nllon, nlat) are ignored.          
        """

        print('loading data... topo')

        if not isinstance(topo, self.mode):
            raise TypeError(f'topo must be {self.mode}')

        topo = topo.squeeze()
        self.tshape = topo.shape

        if self.mode == xr.DataArray:
            self.topo = topo.astype('f4').values
            self.tlons = topo[nlon].values.astype('f4')
            self.tlats = topo[nlat].values.astype('f4')
            
        elif self.mode == np.ndarray:
            self.topo = topo.astype('f4')

            #if tlons == None or flats == None:
            #    raise ValueError(f'coodinates must be set using tlons and tlats options. use np.array.')
            #else:
            self.tlons = tlons.astype('f4')
            self.tlats = tlats.astype('f4')


    def _init_df_old(self, t, lon, lat, lev, u, v, w, z):
        cols = ['lon', 'lat', 'lev', 'u', 'v', 'w', 'z']
        df = pd.DataFrame([], columns=cols, index=self.ttimes)
        df.loc[t] = [lon, lat, lev, u, v, w, z]
        return df


    def _collect_df_old(self, df, t, lon, lat, lev, u, v, w, z):
        df.loc[t] = [lon, lat, lev, u, v, w, z]
        return df


    def _init_df(self, t, lon, lat, lev, u, v, w, z):
        df = pd.DataFrame({'time':[t], 'lon':[lon], 'lat':[lat], 'lev':[lev],
                           'u':[u], 'v':[v], 'w':[w], 'z':[z]})
        return df


    def _collect_df(self, df, t, lon, lat, lev, u, v, w, z):
        _df = pd.DataFrame({'time':[t], 'lon':[lon], 'lat':[lat], 'lev':[lev],
                           'u':[u], 'v':[v], 'w':[w], 'z':[z]})
        return pd.concat([df, _df], ignore_index=True)

    
    def liner_interp_4D(self, u, lons, lats, levs, times, lon, lat, lev, t):
        """ wrapper for bilinear_interp_twice """

        # convert time to float32
        times = [np.datetime64(x, 's') for x in times]
        times = np.array(times).astype('f4')
        t = t.astype('f4')

        return bilinear_interp_twice(u, lons, lats, levs, times, lon, lat, lev, t)


    def values_interp(self, lon, lat, lev, t):

        lons, lats, levs, times = self.lons, self.lats, self.levs, self.times

        if self.parallel:
            u = A_u.reshape(self.ushape)
            v = A_v.reshape(self.ushape)
            w = A_w.reshape(self.ushape)
            z = A_z.reshape(self.ushape)
        else:
            u, v, w, z = self.u, self.v, self.w, self.z

        if self.xrinterp:
            u_int = u.interp(lon=lon, lat=lat, lev=lev, time=t).values
            v_int = v.interp(lon=lon, lat=lat, lev=lev, time=t).values
            w_int = w.interp(lon=lon, lat=lat, lev=lev, time=t).values
            z_int = z.interp(lon=lon, lat=lat, lev=lev, time=t).values
        else:
            u_int =  self.liner_interp_4D(u, lons, lats, levs, times, lon, lat, lev, t)
            v_int =  self.liner_interp_4D(v, lons, lats, levs, times, lon, lat, lev, t)
            w_int =  self.liner_interp_4D(w, lons, lats, levs, times, lon, lat, lev, t)
            z_int =  self.liner_interp_4D(z, lons, lats, levs, times, lon, lat, lev, t)
        return u_int, v_int, w_int, z_int


    def _check_area(self, lon, lat, lev):

        lons, lats, levs = self.lons, self.lats, self.levs

        bool1 = lons.min() < lon < lons.max()
        bool2 = lats.min() < lat < lats.max()
        bool3 = levs.min() < lev < levs.max()

        return bool1 and bool2 and bool3


    def _conv_dig_2m(self, lon, lat, lev, t):

        z = self.z
        lons, lats, levs, times = self.lons, self.lats, self.levs, self.times

        if self.xrinterp:
            height = z.interp(lon=lon, lat=lat, lev=lev, time=t).values
        else:
            height = self.liner_interp_4D(z, lons, lats, levs, times, lon, lat, lev, t)

        if isinstance(self.topo, int):
            topo = 0
        else:
            if self.parallel:
                topo = bilinear_interp(A_topo.reshape(self.tshape), self.tlons, self.tlats, lon, lat)
            else:
                topo = bilinear_interp(self.topo, self.tlons, self.tlats, lon, lat)

        if height - topo < 2.:

            print('track below 2m, search pressure at 2m')

            for l in range(1, 100):
                if self.xrinterp:
                    height = z.interp(lon=lon, lat=lat, lev=lev-l, time=t).values
                else:
                    height = self.liner_interp_4D(z, lons, lats, levs, times, lon, lat, lev-l, t)
                if height > 2.:
                    idx = l
                    break
            lev = lev - idx

        return lev


    def _get_interval_sec(self, times):
        """ get second from freq in pandas.date_range with sign
        positive: foward trajectory
        negative: backward trajectory
        """

        freq = times.freq.freqstr
        base = times.freq.base.name

        try:
            dt = int(re.sub(r"\D", "", freq))
        except ValueError:
            dt = 1

        if freq[0] == '-':
            dt = - dt

        if base == 'H' or base == 'h':  fct = 3600            
        elif base == 'T' or base == 'min':  fct = 60
        elif base == 'S':  fct = 1

        return dt * fct, dt, base, freq


    def est_loc(self, *args):

        if self.ortho:
            return est_loc_orthodrome(*args)
        else:
            return est_loc_independent(*args)


    def _check_undef(self, u, v, w, z):
        return np.isnan(u) or np.isnan(v) or np.isnan(w) or np.isnan(z)


    def _calc_traj_main(self, init_point, number):
        """
        air parcel trajectory using 4th order Runge-Kutta method
        kasuga 2022.07.25

        Parameters
        ----------
        init_point: starting loc. of a parcel

        Return
        ------
        pandas.DataFrame of trajectory parameters

        Trajectory data is also saved in odir named each initial time and loc
        """

        traj_times = self.ttimes

        t0 = np.datetime64(traj_times[0], 's')
        sec, dt, base, freq = self._get_interval_sec(traj_times)
        in1, in2, in3, in4 = True, True, True, True

        lon0, lat0, lev0 = init_point['lon'], init_point['lat'], init_point['lev']
        if not self._check_area(lon0, lat0, lev0):
            print('The initial point is out of the frame')
            return None

        u0, v0, w0, z0 = self.values_interp(lon0, lat0, lev0, t0)
        if self._check_undef(u0, v0, w0, z0):
            print('The initial point has undef values(s)')
            return None

        # name for output
        #oname = f'{t0.year}{t0.month:02}{t0.day:02}{t0.hour:02}{t0.minute:02}_{lev0:.3f}_{lat0:.3f}_{lon0:.3f}_{freq}_{self.method}.csv'
        oname = f'{number}.csv'

        # collecting data
        df = self._init_df(t0, lon0, lat0, lev0, u0, v0, w0, z0)

        for i, t in enumerate(traj_times):

            t_next = np.datetime64(t + pd.Timedelta(dt, base), 's')
            t_half = np.datetime64(t + pd.Timedelta(dt/2, base), 's')

            #di = len(str(len(traj_times)))
            #print(f'{i+1:0{di}}/{len(traj_times)} {t} @ lon={lon0:.2f}, lat={lat0:.2f}, lev={lev0:.2f}')

            lon1k, lat1k, lev1k, lon1, lat1, lev1 = self.est_loc(lon0, lat0, lev0, u0, v0, w0, z0, sec)

            #lev1k = self._conv_dig_2m(lon1k, lat1k, lev1k, t_next)
            #lev1 = self._conv_dig_2m(lon1, lat1, lev1, t_half)

            if self.method == 'e':

                lon_next, lat_next, lev_next = lon1k, lat1k, lev1k

                # final sequence, update variables
                lon0, lat0, lev0 = lon_next, lat_next, lev_next
                u0, v0, w0, z0 = self.values_interp(lon0, lat0, lev0, t_next)
                t0 = t_next
                df = self._collect_df(df, t0, lon0, lat0, lev0, u0, v0, w0, z0)

                continue

            u1, v1, w1, z1 = self.values_interp(lon1, lat1, lev1, t_half)
                
            lon2k, lat2k, lev2k, lon2, lat2, lev2 = self.est_loc(lon0, lat0, lev0, u1, v1, w1, z1, sec)

            #lev2k = self._conv_dig_2m(lon2k, lat2k, lev2k, t_next)
            #lev2 = self._conv_dig_2m(lon2, lat2, lev2, t_half)

            if self.method == 'rk2':

                lon_next, lat_next, lev_next = lon2k, lat2k, lev2k

                # final sequence, update variables
                lon0, lat0, lev0 = lon_next, lat_next, lev_next
                u0, v0, w0, z0 = self.values_interp(lon0, lat0, lev0, t_next)
                t0 = t_next
                df = self._collect_df(df, t0, lon0, lat0, lev0, u0, v0, w0, z0)
                continue

            u2, v2, w2, z2 = self.values_interp(lon2, lat2, lev2, t_half)

            lon3k, lat3k, lev3k, _, _, _ = self.est_loc(lon0, lat0, lev0, u2, v2, w2, z2, sec)

            #lev3k = self._conv_dig_2m(lon3k, lat3k, lev3k, t_next)

            u3, v3, w3, z3 = self.values_interp(lon3k, lat3k, lev3k, t_next)

            lon4k, lat4k, lev4k, _, _, _ = self.est_loc(lon0, lat0, lev0, u3, v3, w3, z3, sec)

            #lev4k = self._conv_dig_2m(lon4k, lat4k, lev4k, t_next)

            lon_next = (lon1k + lon2k*2 + lon3k*2 + lon4k) / 6
            lat_next = (lat1k + lat2k*2 + lat3k*2 + lat4k) / 6
            lev_next = (lev1k + lev2k*2 + lev3k*2 + lev4k) / 6

            in4 = self._check_area(lon_next, lat_next, lev_next)
            if not in4:
                print('parcel goes out of bounds')
                break

            # final sequence, update variables
            lon0, lat0, lev0 = lon_next, lat_next, lev_next
            u0, v0, w0, z0 = self.values_interp(lon0, lat0, lev0, t_next)

            in4u = self._check_undef(u0, v0, w0, z0)
            if in4u:
                print('parameter gets undef')
                break

            t0 = t_next
            df = self._collect_df(df, t0, lon0, lat0, lev0, u0, v0, w0, z0)

        if in4:
            print('success!')

        print('save ', os.path.join(self.odir, oname))
        os.makedirs(self.odir, exist_ok=True)
        df.to_csv(os.path.join(self.odir, oname), index=False, float_format='%g')
        return df


    def calc_traj(self, df, ttimes, method='rk2',
                  ortho=True, odir='.', parallel=False):
        """wrapper for trajectory calculation"""

        self.method = method
        self.parallel = parallel
        self.ortho = ortho
        self.ttimes = ttimes

        if odir != '.':
            self.odir = odir
        
        if method not in ['rk4', 'rk2', 'e']:
            raise ValueError('method must be rk4 (4th order Runge-Kutta) or rk2 (2nd order Runge-Kutta) or e (Euler)')

        if type(self.ttimes) == type(None) and type(ttimes) == type(None):
            raise ValueError('ttimes is not set, use ttimes option or set_times() method')

        if not isinstance(df, pd.Series) and not isinstance(df, pd.DataFrame):
            raise TypeError(f'init_point must be pd.Series or pd.DataFrame but is {type(df)}')

        if parallel:
            raise ValueError('parallel calculation is under construction...')

        print('start trajectory calculation')
        print(f'method: {self.method}')

        if isinstance(df, pd.Series):
            return self._calc_traj_main(df, '0')

        elif isinstance(df, pd.DataFrame) and not parallel:
            for i in df.index:
                print(f'{i+1}/{len(df)} --------')
                pad_len = sum(c.isdigit() for c in str(len(df)))
                self._calc_traj_main(df.iloc[i], f'{i:0{pad_len}}')

        '''elif isinstance(df, pd.DataFrame) and parallel:
            import multiprocessing as mp

            print('making shared memory...')
            A_u = mp.Array('f', self.u.flatten())
            A_v = mp.Array('f', self.v.flatten())
            A_w = mp.Array('f', self.w.flatten())
            A_z = mp.Array('f', self.z.flatten())
            if not isinstance(self.topo, int):
                A_topo = mp.Array('f', self.topo.flatten())

            with mp.Pool(len(df)) as p:
                p.map(self._Runge_Kutta_4o, [df.iloc[i] for i in df.index])'''
