import os, sys
import numpy as np
import numba as nb
import math


@nb.njit
def search_nearest2(coords, target):

    # check target is in coords
    if target < coords.min() or coords.max() < target:
        return 0, 0

    first = True
    for i in range(len(coords)):

        v = coords[i]
        d_new = np.abs(target-v)

        if first:
            first = False
            d = d_new
            ni = i
            continue

        if d_new < d:
            d = d_new
            ni = i

    if ni == 0:
        nii = ni + 1

    elif ni == len(coords)-1:
        nii = ni - 1

    elif np.abs(target-coords[ni+1]) < np.abs(target-coords[ni-1]):
        nii = ni + 1

    else:
        nii = ni - 1

    return ni, nii


@nb.njit
def bilinear_interp_twice(ar, lons, lats, levs, times, _lon, _lat, _lev, _time):
    """
    4D liner interpolation by using bi-linear interpolation twice

    # schema of bi-linear-interpolate
    #  A---|------D
    #  |b        b|
    #  P---X------Q
    #  | c    d   |
    #  |          |
    #  |a        a|
    #  B---|------C
    # then, prepare X for 2lev x 2 tiemstep
    # finaly, do the same bi-linear-intepolate

    Parameters
    ----------
    ar: numpy.ndarray
    lons, lats, levs: coordinates of numpy.ndarray
    times: numpy.ndarray of numpy.datetime64, converted and used as numpy.int64
    _lon, _lat, _lev, _time: location to be interpolated

    Return
    ------
    A interpolated value in ar
    """

    shape = ar.shape

    if len(lons) not in shape or len(lats) not in shape or len(levs) not in shape or len(times) not in shape:
        raise ValueError('lons, lats, levs, and times must have the same coordinate in ar')

    ni, nii = search_nearest2(lons, _lon)
    if ni == nii: return np.nan
    nj, njj = search_nearest2(lats, _lat)
    if nj == njj: return np.nan
    nk, nkk = search_nearest2(levs, _lev)
    if nk == nkk: return np.nan
    nt, ntt = search_nearest2(times, _time)
    if nt == ntt: return np.nan

    a = np.abs(_lat-lats[njj])
    b = np.abs(_lat-lats[nj])
    c = np.abs(_lon-lons[ni])
    d = np.abs(_lon-lons[nii])

    P = np.zeros(4)
    lev_time_list = [[nt, nk],
                     [ntt, nk],
                     [ntt, nkk],
                     [nt, nkk]]

    for i, l in enumerate(lev_time_list):

        A = ar[l[0], l[1], nj, ni]
        B = ar[l[0], l[1], njj, ni]
        C = ar[l[0], l[1], njj, nii]
        D = ar[l[0], l[1], nj, nii]

        P[i] = ((A*a+B*b)*d+(D*a+C*b)*c)/(a+b)/(c+d)

    a = np.abs(_time-times[ntt])
    b = np.abs(_time-times[nt])
    c = np.abs(_lev-levs[nk])
    d = np.abs(_lev-levs[nkk])

    return ((P[0]*a+P[1]*b)*d+(P[3]*a+P[2]*b)*c)/(a+b)/(c+d)


@nb.njit
def bilinear_interp(ar, lons, lats, _lon, _lat):         
    """bi-liner interpolation"""

    shape = ar.shape

    if len(lons) not in shape or len(lats) not in shape:
        raise ValueError('lons, lats, levs, and times must have the same coordinate in ar')

    ni, nii = search_nearest2(lons, _lon)
    nj, njj = search_nearest2(lats, _lat)

    a = np.abs(_lat-lats[njj])
    b = np.abs(_lat-lats[nj])
    c = np.abs(_lon-lons[ni])
    d = np.abs(_lon-lons[nii])

    A = ar[nj, ni]
    B = ar[njj, ni]
    C = ar[njj, nii]
    D = ar[nj, nii]

    return ((A*a+B*b)*d+(D*a+C*b)*c)/(a+b)/(c+d)


@nb.njit
def invert_gcd(lon=135., lat=35., theta=0., d=1000.):
    '''
    invert great circle distance and get new coordinate
    using Spherical Trigonometry
    kasuga 2022.04.10

    Parameters
    ----------
    lon, lat: coordinate of the start point [degree]
    theta: angle from east toward the end point [degree]
    d: travel distance [km]

    Return
    ------
    coordinate of the end point (lon_end, lat_end) [degree]
    '''

    r = 6371.  # earth radius [km]

    A = np.deg2rad(90.-theta)
    c = d/r  # center angle between pos. vectors of start and end [radian]
    b = np.deg2rad(90.-lat)  # zenith of start point

    if b == 0. or b == np.pi or b == -np.pi:
        raise ValueError('error: start point must not be on the North/South Pole')

    cos_a = np.cos(b)*np.cos(c) + np.sin(b)*np.sin(c)*np.cos(A)  # cosine formula of Spherical Trigonometry
    a = np.arccos(cos_a)  # zenith of end point [radian]
    lat_end = 90. - np.rad2deg(a)

    if a == 0.:  # end point is North Pole
        return (0., 90.)
    if a == np.pi or a == -np.pi:  # end point is South Pole
        return (0., -90.)

    cos_C = (np.cos(c) - np.cos(a)*np.cos(b)) / (np.sin(a)*np.sin(b))
    if cos_C > 1.:
        C = 0.
    elif cos_C < -1.:
        C = np.pi / 2.
    else:
        C = np.arccos(cos_C)  # memo: np.arccos() retruns positive value

    if -90. < theta < 90. or 270 < theta:  # travel eastward
        lon_end = lon + np.rad2deg(C)
    else:  # westward
        lon_end = lon - np.rad2deg(C)

    return (lon_end, lat_end)


@nb.njit
def check_lon_0_360(lon):

    if math.isnan(lon):
        return lon

    while True:

        if lon < 0.:
            lon =  lon + 360.

        if lon > 360.:
            lon =  lon - 360.

        if 0. < lon < 360.:
            return lon


@nb.njit
def est_loc_independent(lon0, lat0, lev0, u0, v0, w0, z0, sec):

    r = 6371.2  # earth mean radius km
    circum_lat = 2 * np.pi * r * np.cos(np.deg2rad(lat0))
    circum_lon = 2 * np.pi * r

    distZ = w0 * sec / 100  # hPa / dt hour

    dlon = u0 * sec * 360 / circum_lat / 1000
    dlat = v0 * sec * 360 / circum_lon / 1000

    lonk = lon0 + dlon
    latk = lat0 + dlat
    levk = lev0 + distZ
    lon1 = lon0 + dlon/2
    lat1 = lat0 + dlat/2
    lev1 = lev0 + distZ/2

    lonk = check_lon_0_360(lonk)
    lon1 = check_lon_0_360(lon1)

    return lonk, latk, levk, lon1, lat1, lev1


@nb.njit
def est_loc_orthodrome(lon0, lat0, lev0, u0, v0, w0, z0, sec):

    theta = np.rad2deg(np.arctan2(v0, u0))
    #wp = np.linalg.norm([v0, u0])  # m/s
    wp = (v0**2+u0**2)**(.5)  # m/s

    distXY = abs(wp * sec / 1000)  # km in dt hour
    distZ = w0 * sec / 100  # hPa in dt hour

    levk = lev0 + distZ
    lev1 = lev0 + distZ / 2

    if sec < 0:  # backward
        theta = theta + 180.

    lonk, latk = invert_gcd(lon0, lat0, theta, distXY)
    lon1, lat1 = invert_gcd(lon0, lat0, theta, distXY/2)

    '''
    print(f'wind speed', wp, 'm/s')
    print(f'wind direction', theta, 'deg')
    print(f'horizontal distance', distXY, 'km')
    print(f'vertical velocity', w0, 'Pa/s')
    print(f'vertical distance', distZ, 'hPa')
    print(f'result', lonk)
    print(f'result', latk)
    print(f'result', levk)
    print('--------------------------')
    '''

    return lonk, latk, levk, lon1, lat1, lev1
