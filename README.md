# Atmospheric parcel trajectory using orthodrome

- A python program to calculate air parcel trajectories.
- Parcel locations are located along orthodrome (great circle distance) by solving ordinary derivative equation of motion in porlar coordinate.

<img src="./hoge.png" alt="sample">

## Requirements
Preparing below with conda, and installing the package with pip are recomended.

- Python3/Numpy/Pandas/Xarray/Numba
- Python version 3.7 or higher is recomended. (tested: 3.10, 3.12)

## Install
```
pip install git+https://gitlab.com/kasugab3621/trajortho.git
```

## How to Use

see [run_sample.py](./run_sample.py)
