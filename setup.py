import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="trajortho",
    version="1.2.1",
    author="Satoru Kasuga",
    author_email="kasugab3621@outlook.com",
    description="atmospheric parcel trajectory on orthodrome",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kasugab3621/trajortho.git",
    packages=setuptools.find_packages(),
    install_requires=['numpy', 'pandas', 'xarray', 'numba'],
)
