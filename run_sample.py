
# import windbox class
from trajortho import windbox

# import utils
import xarray as xr
import pandas as pd


# 3D wind fields with enough time space
u = xr.open_dataarray('u_component_of_wind.nc')  # m/s
v = xr.open_dataarray('v_component_of_wind.nc')  # m/s
w = xr.open_dataarray('vertical_velocity.nc')  # Pa/s (not compatible with m/s)
z = xr.open_dataarray('geopotential.nc') / 9.8  # geopotential -> height m

## get coordinates as np.ndarray or pd.DatetimeIndex/np.datetime64
#lats = u['longitude'].values
#lons = u['latitude'].values
#levs = u['level'].values
#times = u['time'].values  # or pd.date_range()
#
## create instance with np.ndarrays of 3D winds and Z (u,v,w,z)
## and Z with coords (times, levs, lats, lons)
## NOTE: arrays must be nested as ['times', 'levs', 'lats', 'lons']
#import numpy as np
#wb = windbox(u, v, w, z,
#             lons=lons, lats=lats, levs=levs, times=times,
#             undef=np.nan)  # np.nan is default

# if netcdf of ERA5 download from CDS
# or other data containing the same attributes
# you can create instance with the simplest way
wb = windbox(u, v, w, z)


# initial parcel locations with formatted DataFrame
# columns must be ['lon', 'lat', 'lev']
# units of values are degree, degree, and hPa, respectively
#df = pd.DataFrame({'lon': [135.,], 'lat': [35.], 'lev': [850.]})  # by hand
df = pd.read_csv('path/to/csvs.csv')  # or csv input


# set time steps to calculate parcel positions using pandas.data_range
# freq units: https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases

# for forward trajectory, set positive freq
traj_times = pd.date_range('2018-02-02 21', '2018-02-05 21', freq='10min')

# for backward trajectory, set negative freq
#traj_times = pd.date_range('2018-02-02 21', '2018-01-00 21', freq='-10min')

# calculate trajectroy analysis, odir is optional or current dir will be used
wb.calc_traj(df, traj_times, odir='OUTPUT_DIR')

# parcels will be saved as {odir}/{numbers}.csv
# {numbers} will be outomatically allocated

# you can use method options
wb.calc_traj(df, traj_times, method='rk4')

# method='rk4': 4th order Runge-Kutta's method
# method='rk2': 2nd order Runge-Kutta's method (default)
# method='e': Euler's method

# trajectroy will be stopped if a parcel reaches ...
# - under 1000 hPa,
# - undef values, or 
# - under topography (only when set topography like below)

# you can use topography as lower boundary (experimental)
topo = xr.open_dataarray('geo_1279l4_0.1x0.1.grib2_v4_unpack.nc') / 9.8  # geopoteintial -> height m
wb.set_topo(topo)

